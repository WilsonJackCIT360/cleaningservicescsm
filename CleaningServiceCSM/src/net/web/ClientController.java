package net.web;
// Based on javaguide.net  usermanagment tutorial
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.dao.ClientDao;
import net.model.Client;


@WebServlet("/")
public class ClientController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ClientDao clientDao;
	
	public void init() {
		clientDao = new ClientDao();
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
		        doGet(request, response);
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
		    throws ServletException, IOException {
		        String action = request.getServletPath();

		        try {
		            switch (action) {
		                case "client/new":
		                    showNewForm(request, response);
		                    break;
		                case "client/insert":
		                    insertClient(request, response);
		                    break;
		                case "client/delete":
		                    deleteClient(request, response);
		                    break;
		                case "client/edit":
		                    showEditForm(request, response);
		                    break;
		                case "client/update":
		                    updateClient(request, response);
		                    break;
		                default:
		                    listClient(request, response);
		                    break;
		            }
		        } catch (SQLException ex) {
		            throw new ServletException(ex);
		        }
		    }

		    private void listClient(HttpServletRequest request, HttpServletResponse response)
		    throws SQLException, IOException, ServletException {
		        List < Client > listClient = clientDao.getAllClient();
		        request.setAttribute("listClient", listClient);
		        RequestDispatcher dispatcher = request.getRequestDispatcher("client-list.jsp");
		        dispatcher.forward(request, response);
		    }

		    private void showNewForm(HttpServletRequest request, HttpServletResponse response)
		    throws ServletException, IOException {
		        RequestDispatcher dispatcher = request.getRequestDispatcher("client-form.jsp");
		        dispatcher.forward(request, response);
		    }

		    private void showEditForm(HttpServletRequest request, HttpServletResponse response)
		    throws SQLException, ServletException, IOException {
		        int id = Integer.parseInt(request.getParameter("id"));
		        Client existingClient = clientDao.getClient(id);
		        RequestDispatcher dispatcher = request.getRequestDispatcher("client-form.jsp");
		        request.setAttribute("client", existingClient);
		        dispatcher.forward(request, response);

		    }

		    private void insertClient(HttpServletRequest request, HttpServletResponse response)
		    throws SQLException, IOException {
		        String name = request.getParameter("name");
		        String street = request.getParameter("street");
		        String cost = request.getParameter("cost");
		        Client newClient = new Client(name, street, cost);
		        clientDao.saveClient(newClient);
		        response.sendRedirect("list");
		    }

		    private void updateClient(HttpServletRequest request, HttpServletResponse response)
		    throws SQLException, IOException {
		        int id = Integer.parseInt(request.getParameter("id"));
		        String name = request.getParameter("name");
		        String street = request.getParameter("street");
		        String cost = request.getParameter("cost");

		        Client client = new Client(id, name, street, cost);
		        clientDao.updateClient(client);
		        response.sendRedirect("list");
		    }

		    private void deleteClient(HttpServletRequest request, HttpServletResponse response)
		    throws SQLException, IOException {
		        int id = Integer.parseInt(request.getParameter("id"));
		        clientDao.deleteClient(id);
		        response.sendRedirect("list");
		    }
	
}
