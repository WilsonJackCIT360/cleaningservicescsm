package net.model;
// Based on javaguide.net  usermanagment tutorial
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="client")

public class Client {
			
		@Id
		@GeneratedValue(strategy= GenerationType.IDENTITY)
		@Column(name = "id")
		protected int id;
		
		@Column(name = "name")
		protected String name;
		
		@Column(name = "street")
		protected String street;
		
		@Column(name = "cost")
		protected String cost;
		
		public Client() {
		}
		
		public Client(String name, String street, String cost) {
			super();
			this.name = name;
			this.street = street;
			this.cost = cost;
		}
		
		public Client(int id, String name, String street, String cost) {
			super();
			this.id = id;
			this.name = name;
			this.street = street;
			this.cost = cost;
		}
		
		public int getId() {
			return id;
		}
		
		public void setId(int id) {
			this.id = id;
		}
		
		public String getName() {
			return name;
		}
		
		public void setName(String name) {
			this.name = name;
		}
		
		public String getStreet() {
			return street;
		}
		
		public void setStreet(String street) {
			this.street = street;
		}
		
		public String getCost() {
			return cost;
		}
		
		public void setCost(String cost) {
			this.cost = cost;
		}
}
