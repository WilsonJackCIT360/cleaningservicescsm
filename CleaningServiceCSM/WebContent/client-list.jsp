<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    
<!DOCTYPE html>
<html>
<head>
 <title>Cleaning Services by M Clients</title>
</head>
<body>
 <center>
  <h1>Clients</h1>
        <h2>
         <a href="new">Add New Client</a>
         &nbsp;&nbsp;&nbsp;
         <a href="list">List All Clients</a>
         
        </h2>
 </center>
    <div align="center">
        <table border="1" cellpadding="5">
            <caption><h2>List of all Clients</h2></caption>
            <tr>
                <th>ID</th>
                <th>Full Name</th>
                <th>Address</th>
                <th>Cost</th>
                <th>Actions</th>
            </tr>
            <c:forEach var="client" items="${listClient}">
                <tr>
                    <td><c:out value="${client.id}" /></td>
                    <td><c:out value="${client.name}" /></td>
                    <td><c:out value="${client.street}" /></td>
                    <td><c:out value="${client.cost}" /></td>
                    <td>
                     <a href="edit?id=<c:out value='${client.id}' />">Edit</a>
                     &nbsp;&nbsp;&nbsp;&nbsp;
                     <a href="delete?id=<c:out value='${client.id}' />">Delete</a>                     
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div> 
</body>
</html>