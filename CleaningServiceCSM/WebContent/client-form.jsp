<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    
<!DOCTYPE html>
<html>
<head>
 <title>CSBYM</title>
</head>
<body>
 <center>
  <h1>User Management</h1>
        <h2>
         <a href="new">Add New Client</a>
         &nbsp;&nbsp;&nbsp;
         <a href="list">List All Clients</a>
         
        </h2>
 </center>
    <div align="center">
  <c:if test="${client != null}">
   <form action="update" method="post">
        </c:if>
        <c:if test="${client == null}">
   <form action="insert" method="post">
        </c:if>
        <table border="1" cellpadding="5">
            <caption>
             <h2>
              <c:if test="${client != null}">
               Edit Client
              </c:if>
              <c:if test="${client == null}">
               Add New Client
              </c:if>
             </h2>
            </caption>
          <c:if test="${client != null}">
           <input type="hidden" name="id" value="<c:out value='${client.id}' />" />
          </c:if>            
            <tr>
                <th>Client Name: </th>
                <td>
                 <input type="text" name="name" size="45"
                   value="<c:out value='${client.name}' />"
                  />
                </td>
            </tr>
            <tr>
                <th>Client Address: </th>
                <td>
                 <input type="text" name="street" size="45"
                   value="<c:out value='${client.street}' />"
                 />
                </td>
            </tr>
            <tr>
                <th>Cleaning Cost: </th>
                <td>
                 <input type="text" name="cost" size="15"
                   value="<c:out value='${client.cost}' />"
                 />
                </td>
            </tr>
            <tr>
             <td colspan="2" align="center">
              <input type="submit" value="Save" />
             </td>
            </tr>
        </table>
        </form>
    </div> 
</body>
</html>