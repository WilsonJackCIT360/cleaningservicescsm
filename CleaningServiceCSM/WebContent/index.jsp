<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>CSBYM</title>
</head>
<body>
<div id="page"> 
    <div id="header">
    	<div class="title">Cleaning Services By M</div>
            </div>
    <div id="bar">
        <div class="menuLink"><a href="index.html">Home</a></div>
        <div class="menuLink"><a href="index.html">Services</a></div>
        <div class="menuLink"><a href="index.html">Contact</a></div>
    </div>
    <div id="pageContent">
    
    
        
      <div class="articleContent">
      
      <div class="rightLinks">
            <div class="linkTitle">Links:</div>
    			<p class="links">
                <a href="http://localhost:8080/CleaningServiceCSM/login.jsp">Employee Login</a><br />
                <a href="http://localhost:8080/CleaningServiceCSM/registration.jsp">Employee Registration</a><br />
                </p>
        </div>
          <p>We Guarantee You’ll Love Your House!</p><br />
          <p>Coming home to a house that’s clean and comfortable gives us all a wonderful feeling. For 15 years we’ve performed housekeeping services according to the wishes of our clients.</p><br />
          <p>With Cleaning Services by M, you get guaranteed results whether you want weekly, bi-weekly or just a one-time house cleaning, we’ll design your cleaning service around your needs. We understand that hiring a cleaning service can be a big decision, and we will work with you to ensure your ‘hot spots’ are cleaned the way you want them done, on time, every time.</p>
	</div>


    </div>

</div>
    
        
</body>
</html>